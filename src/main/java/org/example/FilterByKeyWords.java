package src.main.java.org.example;

import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;

import java.util.List;

public class FilterByKeyWords implements FilterFunction<JsonNode> {
    private final List< String > filterKeyWords;

    public FilterByKeyWords(List < String > filterKeyWords) {
        this.filterKeyWords = filterKeyWords;
    }

    public boolean filter(JsonNode node) {
        if (!node.has("text"))
            return false;
        // keep tweets mentioning keywords
        String tweet = node.get("text").asText().toLowerCase();

        // return only tweets mentioning keywords
        return filterKeyWords.parallelStream().anyMatch(tweet::contains);
    }
}