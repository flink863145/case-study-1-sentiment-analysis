package src.main.java.org.example;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;

public class ExtractTweetSource implements MapFunction<JsonNode, Tuple2< String, JsonNode >> {
    public Tuple2 < String,
            JsonNode > map(JsonNode node) {
        String source = "";
        if (node.has("source")) {
            String sourceHtml = node.get("source").asText().toLowerCase();
            if (sourceHtml.contains("ipad") || sourceHtml.contains("iphone"))
                source = "AppleMobile";
            else if (sourceHtml.contains("mac"))
                source = "AppleMac";
            else if (sourceHtml.contains("android"))
                source = "Android";
            else if (sourceHtml.contains("BlackBerry"))
                source = "BlackBerry";
            else if (sourceHtml.contains("web"))
                source = "Web";
            else
                source = "Other";
        }
        return new Tuple2 < String, JsonNode > (source, node); // returns  (Android,tweetContent)
    }
}