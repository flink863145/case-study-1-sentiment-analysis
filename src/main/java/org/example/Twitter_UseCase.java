package src.main.java.org.example;/* java imports */
import java.util.List;
import java.util.Arrays;
import java.util.Properties;
/* flink imports */
import org.apache.flink.util.Collector;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.FlatMapFunction;
/* parser imports */
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;
/* flink streaming twittter imports */
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.connectors.twitter.TwitterSource;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

//import org.apache.flink.api.java.tuple.Tuple2;

import org.apache.flink.core.fs.Path;
import org.apache.flink.api.common.serialization.SimpleStringEncoder;
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink;
import org.apache.flink.streaming.api.functions.sink.filesystem.rollingpolicies.DefaultRollingPolicy;
/*
A healthcare company is launching an app to give live status of some pollution parameters.
Problem: on which platform the company should launch this app first!?
 */
public class Twitter_UseCase {

  public static void main(String[] args) throws Exception {

    // defining the population interests
    final List < String > keywords = Arrays.asList("global warming", "pollution", "save earth", "temperature increase", "weather change",
      "climate", "co2", "air quality", "dust", "carbondioxide", "greenhouse", "ozone", "methane", "sealevel", "sea level");

    final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

    // Twitter API config
    Properties twitterCredentials = new Properties();
    twitterCredentials.setProperty(TwitterSource.CONSUMER_KEY, "AJcUxpyVsUOk");
    twitterCredentials.setProperty(TwitterSource.CONSUMER_SECRET, "8XzIDXlVLQANLu2OYSfhEqFsSEKI12");
    twitterCredentials.setProperty(TwitterSource.TOKEN, "183234343-U6hOdLRXvWbGAMGZ5ZfSL");
    twitterCredentials.setProperty(TwitterSource.TOKEN_SECRET, "S5m6OEZ7qtgJV52t47Qt7Ze");

    // Twitter data source
    DataStream < String > twitterData = env.addSource(new TwitterSource(twitterCredentials));

    // getting Json response from Twitter API
    DataStream < JsonNode > parsedData = twitterData.map(new TweetParser());

    // filtering only English language profiles
    DataStream < JsonNode > englishTweets = parsedData.filter(new EnglishFilter());

    // filtering english profiles based on population interests
    DataStream < JsonNode > RelevantTweets = englishTweets.filter(new FilterByKeyWords(keywords));

    // Format: <source, tweetObject>
    // getting the device source of the tweets! applying map operation
    DataStream < Tuple2 < String, JsonNode >> tweetsBySource = RelevantTweets.map(new ExtractTweetSource());

    // Format: <source, hourOfDay, 1>
    // applying map operation
    tweetsBySource.map(new ExtractHourOfDay())
      .keyBy(t -> Tuple2.of(t.f0, t.f1)) // groupBy source and hour
      .sum(2) // sum for each category i.e. Number of tweets from 'source' in given 'hour'
      .addSink(StreamingFileSink
        .forRowFormat(new Path("/home/myvm/Flink/sentiment_analysis/src/main/resources/tweets.txt"),
          new SimpleStringEncoder < Tuple3 < String, String, Integer >> ("UTF-8"))
        .withRollingPolicy(DefaultRollingPolicy.builder().build())
        .build());
    //.writeAsText("/home/myvm/Flink/sentiment_analysis/src/main/resources/tweets.txt");
    // e.g. 100 tweets from Android about Pollution in 16th hour of day
    //      150 tweets from Apple devices about Pollution in 20th hour of day etc.

    env.execute("Twitter Analysis");
  }

}