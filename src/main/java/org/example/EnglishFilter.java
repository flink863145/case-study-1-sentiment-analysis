package src.main.java.org.example;

import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;

public class EnglishFilter implements FilterFunction<JsonNode> {
    public boolean filter(JsonNode node) {
        boolean isEnglish =
                node.has("user") &&
                        node.get("user").has("lang") &&
                        node.get("user").get("lang").asText().equals("en");
        return isEnglish;
    }
}