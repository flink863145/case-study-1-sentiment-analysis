package src.main.java.org.example;


import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.JsonNode;
import org.apache.flink.shaded.jackson2.com.fasterxml.jackson.databind.ObjectMapper;

// top level class cannot be static
public class TweetParser implements MapFunction< String, JsonNode> {

    public JsonNode map(String value) throws Exception {
        ObjectMapper jsonParser = new ObjectMapper();

        JsonNode node = jsonParser.readValue(value, JsonNode.class);
        return node;
    }
}